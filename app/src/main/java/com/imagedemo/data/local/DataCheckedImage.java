package com.imagedemo.data.local;

import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

@Entity(primaryKeys = {"key", "url"})
public class DataCheckedImage {
    @NonNull
    private String key;
    @NonNull
    private String url;

    public @NonNull String getKey() {
        return key;
    }

    public void setKey(@NonNull String key) {
        this.key = key;
    }

    public @NonNull String getUrl() {
        return url;
    }

    public void setUrl(@NonNull String url) {
        this.url = url;
    }
}
