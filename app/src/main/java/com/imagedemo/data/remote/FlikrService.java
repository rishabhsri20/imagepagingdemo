package com.imagedemo.data.remote;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface FlikrService {
    @GET("?method=flickr.photos.search&api_key=363f2369bda28f8a2224747561110563&format=json&nojsoncallback=?")
    Call<ImageResponseEntity> searchImages(@Query("text") String searchQuery, @Query("page") int page, @Query("per_page") int perPage, @Query("extras") String str);
}
