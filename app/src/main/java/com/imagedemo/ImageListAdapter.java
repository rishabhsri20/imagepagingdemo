package com.imagedemo;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.imagedemo.components.SquareImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.ImageViewHolder> {
    private List<Image> list;

    ImageListAdapter(List<Image> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.listitem_imageslist, parent, false);
        return new ImageViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
        Image image = list.get(position);
        Glide.with(holder.mImageIv)
                .load(image.getImageUrl())
                .apply(new RequestOptions().error(R.mipmap.ic_launcher).placeholder(R.mipmap.ic_launcher))
                .into(holder.mImageIv);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void refreshData(List<Image> images) {
        list = images;
        notifyDataSetChanged();
    }

    static class ImageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        SquareImageView mImageIv;

        ImageViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}