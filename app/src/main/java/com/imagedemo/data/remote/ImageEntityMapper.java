package com.imagedemo.data.remote;


import com.imagedemo.Image;

import java.util.ArrayList;
import java.util.List;

public class ImageEntityMapper {


    public static List<Image> map(ImageResponseEntity responseEntity) {
        List<Image> list = new ArrayList<>();

        for (ImageResponseEntity.PhotosBean.PhotoBean photoBean : responseEntity.getPhotos().getPhoto()) {
            list.add(new Image(photoBean.getUrl()));
        }
        return list;
    }
}
