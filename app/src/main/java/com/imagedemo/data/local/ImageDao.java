package com.imagedemo.data.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface ImageDao {
    @Query("SELECT * FROM datacheckedimage WHERE `key`=:keyword LIMIT :offset,:limit")
    Flowable<List<DataCheckedImage>> getImages(String keyword, int limit, int offset);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(List<DataCheckedImage> images);
}
