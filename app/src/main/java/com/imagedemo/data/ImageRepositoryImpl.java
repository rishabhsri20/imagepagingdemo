package com.imagedemo.data;

import android.content.Context;

import com.imagedemo.Image;

import java.util.List;

import io.reactivex.Observable;

public class ImageRepositoryImpl implements ImageRepository {

    private final DataSourceFactory factory;

    public ImageRepositoryImpl(Context context) {
        this.factory = new DataSourceFactory(context);
    }

    @Override
    public Observable<List<Image>> searchImages(String keyword, int pageNo) {
        return factory.getDataSource().searchImages(keyword, pageNo);
    }
}
