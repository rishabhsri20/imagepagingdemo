package com.imagedemo;

import android.arch.persistence.room.Entity;

@Entity(tableName = "image")
public class Image {
    public Image(String url) {
        this.imageUrl = url;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    private String imageUrl;
}
