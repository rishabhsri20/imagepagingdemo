package com.imagedemo;

import android.arch.lifecycle.Observer;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchImageFragment extends Fragment implements SearchView.OnQueryTextListener, Toolbar.OnMenuItemClickListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.imagelist_recyclerview)
    RecyclerView mImageListRecyclerView;

    @BindView(R.id.nodata_textview)
    TextView mNoDataTextView;

    @BindView(R.id.progress_progressbar)
    ProgressBar mProgressPrg;

    private SearchView mSearchView;
    private ImageListAdapter imageListAdapter;
    private ImageViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_image, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initComponents();
        setActionListener();
    }

    private void initComponents() {
        toolbar.inflateMenu(R.menu.menu_home);
        setSearchView();
        setGridAdapter();

        viewModel.getImages().observe(this, new Observer<List<Image>>() {
            @Override
            public void onChanged(@Nullable List<Image> images) {
                imageListAdapter.refreshData(images);
            }
        });
        viewModel.getShowLoader().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                mProgressPrg.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
            }
        });
        viewModel.getShowGridSwitch().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                toolbar.getMenu().findItem(R.id.action_gridsize).setVisible(aBoolean);
                mNoDataTextView.setVisibility(!aBoolean ? View.VISIBLE : View.GONE);
            }
        });
        viewModel.getHasError().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {

            }
        });
    }

    public void setSearchView() {
        mSearchView = (SearchView) toolbar.getMenu().findItem(R.id.action_search).getActionView();
        mSearchView.setQueryHint(getString(R.string.text_search_hint));
        EditText mSearchEdt = mSearchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        mSearchEdt.setTextColor(Color.WHITE);
        mSearchEdt.setHintTextColor(Color.WHITE);
    }

    private void setGridAdapter() {
        imageListAdapter = new ImageListAdapter(new ArrayList<Image>());
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
        mImageListRecyclerView.setLayoutManager(layoutManager);
        mImageListRecyclerView.setAdapter(imageListAdapter);
    }

    private void setActionListener() {
        toolbar.setOnMenuItemClickListener(this);
        mSearchView.setOnQueryTextListener(this);
        mImageListRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager layoutManager = (LinearLayoutManager)
                        recyclerView.getLayoutManager();
                int lastPosition;
                if (layoutManager != null) {
                    lastPosition = layoutManager
                            .findLastVisibleItemPosition();
                    if (lastPosition == imageListAdapter.getItemCount() - 1) {
                        viewModel.loadMore();
                    }
                }
            }
        });
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        viewModel.onSearchPerformed(s);
        toolbar.setTitle(s);
        toolbar.getMenu().findItem(R.id.action_search).collapseActionView();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }

    public void setViewModel(ImageViewModel viewModel) {
        this.viewModel = viewModel;
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.size2:
                ((GridLayoutManager) Objects.requireNonNull(mImageListRecyclerView.getLayoutManager())).setSpanCount(2);
                break;
            case R.id.size3:
                ((GridLayoutManager) Objects.requireNonNull(mImageListRecyclerView.getLayoutManager())).setSpanCount(3);
                break;
            case R.id.size4:
                ((GridLayoutManager) Objects.requireNonNull(mImageListRecyclerView.getLayoutManager())).setSpanCount(4);
                break;
        }
        return false;
    }
}