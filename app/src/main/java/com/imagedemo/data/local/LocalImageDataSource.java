package com.imagedemo.data.local;

import com.imagedemo.Image;
import com.imagedemo.data.ImageDataSource;
import com.imagedemo.data.remote.ClouldImageDataSource;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

public class LocalImageDataSource implements ImageDataSource {
    private final ImageDao imageDao;

    public LocalImageDataSource(ImageDao imageDao) {
        this.imageDao = imageDao;
    }

    @Override
    public Observable<List<Image>> searchImages(String keyword, int pageNo) {
        int offset = ClouldImageDataSource.PAGINATION_LIMIT * (pageNo - 1);
        return imageDao.getImages(keyword, pageNo, offset).map(new Function<List<DataCheckedImage>, List<Image>>() {
            @Override
            public List<Image> apply(List<DataCheckedImage> dataCheckedImages) {
                List<Image> images = new ArrayList<>();
                if (dataCheckedImages != null) {
                    for (DataCheckedImage dataCheckedImage : dataCheckedImages) {
                        images.add(new Image(dataCheckedImage.getUrl()));
                    }
                }
                return images;
            }
        }).toObservable();
    }
}
