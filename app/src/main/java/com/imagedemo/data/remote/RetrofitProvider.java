package com.imagedemo.data.remote;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitProvider {

    private static final String BASE_URL = "https://api.flickr.com/services/rest/";
    private Retrofit retrofit;

    RetrofitProvider() {
        retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();
    }

    public FlikrService createService() {
        return retrofit.create(FlikrService.class);
    }
}
