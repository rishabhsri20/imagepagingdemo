package com.imagedemo;

import android.content.Context;

import com.imagedemo.data.ImageRepository;
import com.imagedemo.data.ImageRepositoryImpl;

public class Injector {

    public static ImageRepository inject(Context context) {
        return new ImageRepositoryImpl(context);
    }
}
