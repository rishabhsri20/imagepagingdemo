package com.imagedemo.data.remote;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ImageResponseEntity {

    /**
     * photos : {"page":1,"pages":13402,"perpage":10,"total":"134014","photo":[{"id":"41879819160","owner":"153431116@N08","secret":"07b7eab1e9","server":"930","farm":1,"title":"Bayu Skak Memungkinkan Torch Relay Game Asia 2018 di Banjarmasin - Merdeka Sport","ispublic":1,"isfriend":0,"isfamily":0},{"id":"29815354988","owner":"153965003@N06","secret":"c1436858bc","server":"941","farm":1,"title":"ask design CREATIVE CONCEPTS","ispublic":1,"isfriend":0,"isfamily":0},{"id":"28796051897","owner":"153965003@N06","secret":"bcbd1d244e","server":"852","farm":1,"title":"ask design CREATIVE CONCEPTS","ispublic":1,"isfriend":0,"isfamily":0},{"id":"41876955660","owner":"153965003@N06","secret":"9a145556b2","server":"860","farm":1,"title":"ask design CREATIVE CONCEPTS","ispublic":1,"isfriend":0,"isfamily":0},{"id":"28796051677","owner":"153965003@N06","secret":"1ddf05439b","server":"858","farm":1,"title":"ask design CREATIVE CONCEPTS","ispublic":1,"isfriend":0,"isfamily":0},{"id":"41876955540","owner":"153965003@N06","secret":"6c6e2d368c","server":"849","farm":1,"title":"ask design CREATIVE CONCEPTS","ispublic":1,"isfriend":0,"isfamily":0},{"id":"43638389752","owner":"153965003@N06","secret":"3e2115d393","server":"942","farm":1,"title":"ask design CREATIVE CONCEPTS","ispublic":1,"isfriend":0,"isfamily":0},{"id":"28796051507","owner":"153965003@N06","secret":"53e80ed85c","server":"938","farm":1,"title":"ask design CREATIVE CONCEPTS","ispublic":1,"isfriend":0,"isfamily":0},{"id":"43638389472","owner":"153965003@N06","secret":"8bf640d324","server":"936","farm":1,"title":"ask design CREATIVE CONCEPTS","ispublic":1,"isfriend":0,"isfamily":0},{"id":"41876955220","owner":"153965003@N06","secret":"fd4ae7483b","server":"851","farm":1,"title":"ask design CREATIVE CONCEPTS","ispublic":1,"isfriend":0,"isfamily":0}]}
     * stat : ok
     */
    @SerializedName("photos")
    private PhotosBean photos;
    @SerializedName("stat")
    private String stat;

    public PhotosBean getPhotos() {
        return photos;
    }

    public void setPhotos(PhotosBean photos) {
        this.photos = photos;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public static class PhotosBean {
        /**
         * page : 1
         * pages : 13402
         * perpage : 10
         * total : 134014
         * photo : [{"id":"41879819160","owner":"153431116@N08","secret":"07b7eab1e9","server":"930","farm":1,"title":"Bayu Skak Memungkinkan Torch Relay Game Asia 2018 di Banjarmasin - Merdeka Sport","ispublic":1,"isfriend":0,"isfamily":0},{"id":"29815354988","owner":"153965003@N06","secret":"c1436858bc","server":"941","farm":1,"title":"ask design CREATIVE CONCEPTS","ispublic":1,"isfriend":0,"isfamily":0},{"id":"28796051897","owner":"153965003@N06","secret":"bcbd1d244e","server":"852","farm":1,"title":"ask design CREATIVE CONCEPTS","ispublic":1,"isfriend":0,"isfamily":0},{"id":"41876955660","owner":"153965003@N06","secret":"9a145556b2","server":"860","farm":1,"title":"ask design CREATIVE CONCEPTS","ispublic":1,"isfriend":0,"isfamily":0},{"id":"28796051677","owner":"153965003@N06","secret":"1ddf05439b","server":"858","farm":1,"title":"ask design CREATIVE CONCEPTS","ispublic":1,"isfriend":0,"isfamily":0},{"id":"41876955540","owner":"153965003@N06","secret":"6c6e2d368c","server":"849","farm":1,"title":"ask design CREATIVE CONCEPTS","ispublic":1,"isfriend":0,"isfamily":0},{"id":"43638389752","owner":"153965003@N06","secret":"3e2115d393","server":"942","farm":1,"title":"ask design CREATIVE CONCEPTS","ispublic":1,"isfriend":0,"isfamily":0},{"id":"28796051507","owner":"153965003@N06","secret":"53e80ed85c","server":"938","farm":1,"title":"ask design CREATIVE CONCEPTS","ispublic":1,"isfriend":0,"isfamily":0},{"id":"43638389472","owner":"153965003@N06","secret":"8bf640d324","server":"936","farm":1,"title":"ask design CREATIVE CONCEPTS","ispublic":1,"isfriend":0,"isfamily":0},{"id":"41876955220","owner":"153965003@N06","secret":"fd4ae7483b","server":"851","farm":1,"title":"ask design CREATIVE CONCEPTS","ispublic":1,"isfriend":0,"isfamily":0}]
         */
        @SerializedName("page")
        private int page;
        @SerializedName("pages")
        private int pages;
        @SerializedName("perpage")
        private int perpage;
        @SerializedName("total")
        private String total;
        @SerializedName("photo")
        private List<PhotoBean> photo;

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getPerpage() {
            return perpage;
        }

        public void setPerpage(int perpage) {
            this.perpage = perpage;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public List<PhotoBean> getPhoto() {
            return photo;
        }

        public void setPhoto(List<PhotoBean> photo) {
            this.photo = photo;
        }

        public static class PhotoBean {
            /**
             * id : 41879819160
             * owner : 153431116@N08
             * secret : 07b7eab1e9
             * server : 930
             * farm : 1
             * title : Bayu Skak Memungkinkan Torch Relay Game Asia 2018 di Banjarmasin - Merdeka Sport
             * ispublic : 1
             * isfriend : 0
             * isfamily : 0
             */
            @SerializedName("id")
            private String id;
            @SerializedName("owner")
            private String owner;
            @SerializedName("secret")
            private String secret;
            @SerializedName("server")
            private String server;
            @SerializedName("farm")
            private int farm;
            @SerializedName("title")
            private String title;
            @SerializedName("ispublic")
            private int ispublic;
            @SerializedName("isfriend")
            private int isfriend;
            @SerializedName("isfamily")
            private int isfamily;
            @SerializedName("url_m")
            private String url;

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getOwner() {
                return owner;
            }

            public void setOwner(String owner) {
                this.owner = owner;
            }

            public String getSecret() {
                return secret;
            }

            public void setSecret(String secret) {
                this.secret = secret;
            }

            public String getServer() {
                return server;
            }

            public void setServer(String server) {
                this.server = server;
            }

            public int getFarm() {
                return farm;
            }

            public void setFarm(int farm) {
                this.farm = farm;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public int getIspublic() {
                return ispublic;
            }

            public void setIspublic(int ispublic) {
                this.ispublic = ispublic;
            }

            public int getIsfriend() {
                return isfriend;
            }

            public void setIsfriend(int isfriend) {
                this.isfriend = isfriend;
            }

            public int getIsfamily() {
                return isfamily;
            }

            public void setIsfamily(int isfamily) {
                this.isfamily = isfamily;
            }
        }
    }
}
