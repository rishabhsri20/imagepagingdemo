package com.imagedemo.data;

import com.imagedemo.Image;

import java.util.List;

import io.reactivex.Observable;

public interface ImageDataSource {

    Observable<List<Image>> searchImages(String keyword, int pageNo);
}
