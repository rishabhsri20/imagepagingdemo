package com.imagedemo.data.remote;

import com.imagedemo.Image;
import com.imagedemo.data.ImageDataSource;
import com.imagedemo.data.local.DataCheckedImage;
import com.imagedemo.data.local.ImageDao;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import retrofit2.Response;

public class ClouldImageDataSource implements ImageDataSource {

    public static final int PAGINATION_LIMIT = 50;
    private final ImageDao imageDao;
    private FlikrService service;

    public ClouldImageDataSource(ImageDao imageDao) {
        this.imageDao = imageDao;
        service = new RetrofitProvider().createService();
    }

    @Override
    public Observable<List<Image>> searchImages(final String keyword, final int pageNo) {
        return Observable.create(new ObservableOnSubscribe<ImageResponseEntity>() {
            @Override
            public void subscribe(ObservableEmitter<ImageResponseEntity> emitter) throws Exception {
                Response<ImageResponseEntity> imageResponseEntityResponse = service.searchImages(keyword, pageNo, PAGINATION_LIMIT, "url_m").execute();
                if (imageResponseEntityResponse.isSuccessful()) {
                    emitter.onNext(imageResponseEntityResponse.body());
                    emitter.onComplete();
                } else {
                    emitter.onError(new Exception("Could not reach to server"));
                }
            }
        }).map(new Function<ImageResponseEntity, List<Image>>() {
            @Override
            public List<Image> apply(ImageResponseEntity responseEntity) throws Exception {
                return ImageEntityMapper.map(responseEntity);
            }
        }).doOnNext(new Consumer<List<Image>>() {
            @Override
            public void accept(List<Image> imageResponse) throws Exception {
                if (imageResponse != null) {
                    List<DataCheckedImage> cachedImages = new ArrayList<>(imageResponse.size());
                    for (Image image : imageResponse) {
                        DataCheckedImage checkedImage = new DataCheckedImage();
                        checkedImage.setKey(keyword);
                        checkedImage.setUrl(image.getImageUrl());
                        cachedImages.add(checkedImage);
                    }
                    imageDao.insert(cachedImages);
                }
            }
        });
    }
}
