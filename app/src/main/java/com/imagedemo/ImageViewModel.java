package com.imagedemo;

import android.app.Application;
import android.arch.core.util.Function;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;

import com.imagedemo.data.ImageRepository;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class ImageViewModel extends AndroidViewModel {
    private MutableLiveData<List<Image>> images = new MutableLiveData<>();
    private ImageRepository imageRepository;
    private int pageNo;
    private MutableLiveData<String> searchTerm = new MutableLiveData<>();
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private MutableLiveData<Boolean> hasError = new MutableLiveData<>();
    private MutableLiveData<Boolean> showLoader = new MutableLiveData<>();
    private LiveData<Boolean> showGridSwitch = Transformations.map(images, new Function<List<Image>, Boolean>() {
        @Override
        public Boolean apply(List<Image> input) {
            return input != null && !input.isEmpty();
        }
    });

    public ImageViewModel(@NonNull Application application) {
        super(application);
        imageRepository = Injector.inject(application.getApplicationContext());
    }

    public void onSearchPerformed(String keyword) {
        showLoader.setValue(true);
        pageNo = 1;
        searchTerm.setValue(keyword);
        images.setValue(new ArrayList<Image>());
        fetchImages();
    }

    public void loadMore() {
        pageNo++;
        fetchImages();
    }

    @Override
    protected void onCleared() {
        compositeDisposable.dispose();
        super.onCleared();
    }

    private void fetchImages() {
        DisposableObserver<List<Image>> disposableObserver = imageRepository.searchImages(searchTerm.getValue(), pageNo)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<List<Image>>() {
                    @Override
                    public void onNext(List<Image> images) {
                        List<Image> value = ImageViewModel.this.images.getValue();
                        if (value != null) {
                            value.addAll(images);
                        }
                        ImageViewModel.this.images.setValue(value);
                        showLoader.setValue(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        hasError.setValue(true);
                        showLoader.setValue(false);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
        compositeDisposable.add(disposableObserver);

    }

    public LiveData<List<Image>> getImages() {
        return images;
    }

    public MutableLiveData<Boolean> getHasError() {
        return hasError;
    }

    public LiveData<Boolean> getShowGridSwitch() {
        return showGridSwitch;
    }

    public MutableLiveData<Boolean> getShowLoader() {
        return showLoader;
    }
}
