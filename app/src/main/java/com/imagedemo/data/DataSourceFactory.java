package com.imagedemo.data;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.imagedemo.data.local.ImageDao;
import com.imagedemo.data.local.ImageDatabase;
import com.imagedemo.data.local.LocalImageDataSource;
import com.imagedemo.data.remote.ClouldImageDataSource;
import com.imagedemo.data.remote.ConnectivityCheck;

public class DataSourceFactory {

    private final Context context;
    private final ImageDao imageDao;

    DataSourceFactory(Context context) {
        this.context = context;
        ImageDatabase db = Room.databaseBuilder(context.getApplicationContext(),
                ImageDatabase.class, "cached_images").build();
        imageDao = db.imageDao();
    }

    public ImageDataSource getDataSource() {
        if(ConnectivityCheck.isConnected(context)) {
            return new ClouldImageDataSource(imageDao);
        } else {
            return new LocalImageDataSource(imageDao);
        }
    }
}
